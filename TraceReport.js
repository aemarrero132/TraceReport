let express = require('express'),
  app = express(),
  port = 3000,
  sdk = express(),
  appDirectory = __dirname + '/app/webapp',
  launchUrl = 'http://localhost:' + port + appDirectory;

  /**Para la correr la app con el sdk local
  	*sdkDirectory = __dirname + '/sdk',
  	*sdkURL = 'http://localhost:' + port + sdkDirectory;
  	*app.use('/sdk', express.static(sdkDirectory));
  **/

app.use('/TraceReport', express.static(appDirectory));

// start server
app.listen(port);

// log to server console
console.log("SAPUI5 app TraceRepor running at\n  => " + launchUrl +
  " \nCTRL + C to shutdown");

/**Para usar la App correr en el navegador http://localhost:3000/SecondWeight
	* Debe tener en la misma carpeta el modulo express node_modules **/
