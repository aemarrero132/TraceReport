sap.ui.define([], function() {
  "use strict";

  return {

    status : function(status) {
      var result;
      switch (status) {
        case "11":
            result = "Carga";
            break;
        case "1":
            result = "Primera Pesada";
            break;
        case "3":
            result = "Rechazado";
            break;
        case "4":
            result = "Descarga";
            break;
        case "5":
            result = "Bloqueado";
            break;
        case "7":
            result = "Desbloqueado";
            break;
        case "8":
            result = "No Aprobado";
            break;
      }
      return result;
    }
  };
});