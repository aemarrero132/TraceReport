sap.ui.define([
	"TraceReport/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"sap/m/MessageToast"
], function(BaseController, Filter, FilterOperator, JSONModel, Device, MessageToast) {
	"use strict";

	return BaseController.extend("TraceReport.controller.Master", {

		onInit: function() {
			var oList = this.getView().byId("__list0");
			this._oList = oList;
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};
			var oViewModel = this._createViewModel();
			this.setModel(oViewModel, "masterView");

			this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);
			
		},
		
		onPress2 : function() {
  			console.log("EVENTO MASTER");
  			/*
  			var oSplitContainer = sap.ui.getCore().byId("oSplitApp");
 			oSplitContainer.setMode(sap.m.SplitAppMode.StretchCompressMode);
  			if(oSplitContainer.isMasterShown()){
 				oSplitContainer.setMode(sap.m.SplitAppMode.HideMode); */
		},

		_onMasterMatched: function(oEvent) {

			var mdticket = this.getView().getModel("data"); 
            const that = this;
            //SE LISTAN LOS TICKETS CON STATUS 5 (BLOQUEADO) y 14 (APROBADO DESPACHO).
            $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "http://localhost:3000/bascule/Z_WGHT_H/", //multistatus/5/14
                dataType: "json",
                async: false,
                success: function(data) {
                	if(data.length > 0) {
	                    mdticket.setProperty("/tickets", []);
	                    mdticket.setProperty("/tickets/", data);

	                    //OBTENGO EL NOMBRE DEL CONDUCTOR DE CADA TICKET
	                    for(var i=0; i < data.length; i++) {
	                    	
	                    	var status = data[i].Z_STATUS;
	                    	mdticket.setProperty("/tickets/"+i+"/Z_STATUS_D/", that.status(status));

	                    	var driverId = data[i].Z_DRIVER;
	                    	$.ajax({
	                    		type: "GET",
				                contentType: "application/json",
				                url: "http://localhost:3000/bascule/Z11_DRIVERS/" + driverId,
				                dataType: "json",
				                async: false,
				                success: function(dataDriver) {
				                	mdticket.setProperty("/tickets/"+i+"/NAME/", dataDriver[0].NAME);
				                	mdticket.setProperty("/tickets/"+i+"/DOC_ID/", dataDriver[0].DOC_ID);
				                	mdticket.setProperty("/tickets/"+i+"/CARRIER_ID/", dataDriver[0].CARRIER_ID);
				                	
				                	var placa = mdticket.getProperty("/tickets/"+ i + "/Z_VEHID");
				                	//OBTENGO INFORMACION DEL VEHICULO SEGUN SU PLACA
				                	$.ajax({
			                    		type: "GET",
						                contentType: "application/json",
						                url: "http://localhost:3000/bascule/Z11_TRUCKS/plate/" + placa,
						                dataType: "json",
						                async: false,
						                success: function(dataTruck) {
						                	console.log("dataTruck", dataTruck);
						                	mdticket.setProperty("/tickets/"+i+"/TRUCK_ID/", dataTruck[0].TRUCK_ID);
						                	mdticket.setProperty("/tickets/"+i+"/TRUCK_BRAND/", dataTruck[0].TRUCK_BRAND);
						                	mdticket.setProperty("/tickets/"+i+"/TRUCK_MODEL/", dataTruck[0].TRUCK_MODEL);
						                	mdticket.setProperty("/tickets/"+i+"/TRUCK_REFWGT/", dataTruck[0].TRUCK_REFWGT);
						                	mdticket.setProperty("/tickets/"+i+"/REFWGTUM/", dataTruck[0].REFWGTUM);
						                },
						                error: function(request, status, error) {
						                    MessageToast.show(status + " : " + error);
						                    console.log("Read failed", error);
						                }
	                    			});

				                	var carrierId = mdticket.getProperty("/tickets/"+ i + "/CARRIER_ID");
	                    			//OBTENGO INFORMACION DEL TRANSPORTISTA SEGUN SU ID
				                	$.ajax({
			                    		type: "GET",
						                contentType: "application/json",
						                url: "http://localhost:3000/bascule/Z11_CARRIERS/" + carrierId,
						                dataType: "json",
						                async: false,
						                success: function(dataCarrier) {
						                	console.log("dataCarrier", dataCarrier);
						                	mdticket.setProperty("/tickets/"+i+"/CARRIER_NAME/", dataCarrier[0].CARRIER_NAME);
						                	mdticket.setProperty("/tickets/"+i+"/FISCAL_ID/", dataCarrier[0].FISCAL_ID);
						                },
						                error: function(request, status, error) {
						                    MessageToast.show(status + " : " + error);
						                    console.log("Read failed", error);
						                }
	                    			});
				                },
				                error: function(request, status, error) {
				                    MessageToast.show(status + " : " + error);
				                    console.log("Read failed", error);
				                }
	                    	});

	                    }
	                    console.log("mdticket", mdticket.oData);
	                }
	                else { MessageToast.show("No Existen Tickets Bloqueados o Pendientes por Despacho", {duration:3000, autoClose: false}); }
                },
                error: function(request, status, error) {
                    MessageToast.show(status + " : " + error);
                    console.log("Read failed", error);
                }
            });
		},

        onSelectionChange : function(oEvent){ 

            var bReplace = !Device.system.phone;
            //Obtengo posicion del item seleccionado de la lista
            var idx = oEvent.getSource()._aSelectedPaths[0].split("/") //Retorna ["", "tickets", "selected_pos"]
            var itemSelected = idx[2]; 
            
            this.getRouter().navTo("detail", { 
                id: itemSelected
            }, bReplace);  
        },
        
        status : function(status) {
		      var result;
		      switch (status) {
		        case "1":
		            result = "Primera Pesada";
		            break;
		        case "3":
		            result = "Rechazado";
		            break;
		        case "4":
		            result = "Descarga";
		            break;
		        case "5":
		            result = "Bloqueado";
		            break;
		        case "7":
		            result = "Desbloqueado";
		            break;
		        case "8":
		            result = "No Aprobado";
		            break;
		        case "11":
		            result = "Carga";
		            break;	
		      }
		      return result;
    	},

		_createViewModel: function() {
			return new JSONModel({
				isFilterBarVisible: false,
				filterBarLabel: "",
				delay: 0,
				title: this.getResourceBundle().getText("masterTitleCount", [0]),
				noDataText: this.getResourceBundle().getText("masterListNoDataText"),
				sortBy: "Name",
				groupBy: "None"
			});
		},
		onSearch: function(oEvent) {

			if (oEvent.getParameters().refreshButtonPressed) {
				this.onRefresh();
				return;
			}
			var sQuery = oEvent.getSource().getValue();
			//var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("Z_TICK", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();

		},
		_applyFilterSearch: function() {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("masterView");
			this._oList.getBinding("items").filter(aFilters, "Application");
			// changes the noDataText of the list in case there are no filter results
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataWithFilterOrSearchText"));
			} else if (this._oListFilterState.aSearch.length > 0) {
				// only reset the no data text to default when no new search was triggered
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
			}
		},
		_updateListItemCount: function(iTotalItems) {
			var sTitle;
			// only update the counter if the length is final
			if (this._oList.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("masterTitleCount", [iTotalItems]);
				this.getModel("masterView").setProperty("/title", sTitle);
			}
		},
		onUpdateFinished: function(oEvent) {
			this._updateListItemCount(oEvent.getParameter("total"));
		},
		onTest(){
			console.log("Controlador Master")
		}
	});
});